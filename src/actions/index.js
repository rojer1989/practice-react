import http from "../api";

let nextUserId = 0;

export const addUser = name => {
    return {
        type: 'ADD_USER',
        id: nextUserId++,
        name
    }
};

const fetchUsersSuccess = users => ({
    type: 'FETCH_USER_SUCCESS',
    payload: users
});

export const fetchUsers = () => dispatch =>
    http('http://localhost:8000/api/v1/')
        .get()
        .then(users => dispatch(fetchUsersSuccess(users)))
        .catch(console.log);
