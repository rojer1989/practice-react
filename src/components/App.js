import React from 'react'
import NotFound from "./NotFound";
import UserPage from "./UserPage";
import {Route, Switch} from "react-router";

const App = () => (
    <Switch>
        <Route exact path='/' component={UserPage}/>
        <Route path="*" component={NotFound}/>
    </Switch>
);

export default App
