import AddUser from "../containers/AddUser";
import UsersList from "./UsersList";
import React, {Component} from 'react';
import Map from "./Map";
import {fetchUsers} from "../actions";


class UserPage extends Component {
    componentDidMount() {
        fetchUsers();
    }

    render() {
        return (
            <div>
                <Map/>
                <AddUser/>
                <UsersList/>
            </div>
        )
    }
}

export default UserPage;
